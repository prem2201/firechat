import Button from "@material-ui/core/Button";
import "firebase/analytics";
import firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";
import React, { useRef, useState } from "react";
import { useAuthState } from "react-firebase-hooks/auth";
import { useCollectionData } from "react-firebase-hooks/firestore";
import { BiSend } from "react-icons/bi";
import { BsChatQuote } from "react-icons/bs";
import "./App.css";
firebase.initializeApp({
  apiKey: "AIzaSyD9bEqYUUbDYJbDjW1iSxYZPUeID2DvEJw",
  authDomain: "firechat-1d327.firebaseapp.com",
  projectId: "firechat-1d327",
  storageBucket: "firechat-1d327.appspot.com",
  messagingSenderId: "213921138841",
  appId: "1:213921138841:web:0665dc1e6af651ddf49008",
  measurementId: "G-VRSXZ4R58D",
});

const auth = firebase.auth();
const firestore = firebase.firestore();

function App() {
  const [user] = useAuthState(auth);

  return (
    <div className="App">
      <header>
        <BsChatQuote style={{ fontSize: "30px" }} />
        <SignOut />
      </header>

      <section>{user ? <ChatRoom /> : <SignIn />}</section>
    </div>
  );
}

function SignIn() {
  const signInWithGoogle = () => {
    const provider = new firebase.auth.GoogleAuthProvider();
    auth.signInWithPopup(provider);
  };

  return (
    <>
      <center>
        <Button
          variant="contained"
          color="primary"
          className="sign-in"
          onClick={signInWithGoogle}
        >
          Sign in with Google
        </Button>
      </center>
    </>
  );
}

function SignOut() {
  return (
    auth.currentUser && (
      <Button
        variant="contained"
        color="secondary"
        onClick={() => auth.signOut()}
      >
        Sign Out
      </Button>
    )
  );
}

function ChatRoom() {
  const dummy = useRef();
  const messagesRef = firestore.collection("messages");
  const query = messagesRef.orderBy("createdAt").limit(25);

  const [messages] = useCollectionData(query, { idField: "id" });

  const [formValue, setFormValue] = useState("");

  const sendMessage = async (e) => {
    e.preventDefault();

    const { uid, photoURL } = auth.currentUser;

    await messagesRef.add({
      text: formValue,
      createdAt: firebase.firestore.FieldValue.serverTimestamp(),
      uid,
      photoURL,
    });

    setFormValue("");
    dummy.current.scrollIntoView({ behavior: "smooth" });
  };

  return (
    <>
      <main>
        {messages &&
          messages.map((msg) => <ChatMessage key={msg.id} message={msg} />)}

        <span ref={dummy}></span>
      </main>

      <form onSubmit={sendMessage}>
        <input
          value={formValue}
          onChange={(e) => setFormValue(e.target.value)}
          placeholder="Enter Message"
        />

        <button type="submit" disabled={!formValue}>
          <BiSend />
        </button>
      </form>
    </>
  );
}

function ChatMessage(props) {
  const { text, uid, photoURL } = props.message;

  const messageClass = uid === auth.currentUser.uid ? "sent" : "received";

  return (
    <>
      <div className={`message ${messageClass}`}>
        <img
          src={
            photoURL || "https://api.adorable.io/avatars/23/abott@adorable.png"
          }
          alt="profile"
        />
        <p>{text}</p>
      </div>
    </>
  );
}

export default App;
